//
//  KeyValueExt.swift
//
//  Created by Diego Belli Mondego on 12/04/15.
//

import Foundation


extension NSObject {
    
    // Creates an object from a dictionary
    class func fromJson(jsonInfo: NSDictionary) -> Self {
        let object = self.init()
        
        (object as NSObject).load(jsonInfo)
        
        return object
    }
    
    func load(jsonInfo: NSDictionary) {
        for (key, value) in jsonInfo {
            let keyName = key as! String
            
            if (respondsToSelector(NSSelectorFromString(keyName)) && !(value is NSNull)) {
                
                processValue(value, forKey: keyName)
            } else {
                let camelCaseName = underscoreToCamelCase(keyName)
                if (respondsToSelector(NSSelectorFromString(camelCaseName)) && !(value is NSNull)) {
                    processValue(value, forKey: camelCaseName)
                }
            }
        }
    }
    
    
    func processValue(value: AnyObject?, forKey key: String){
    
        if value is NSDictionary {
        
            processObject(value, forKey: key)
        }
        else if value is NSArray && value != nil{
            
            var array : [AnyObject]! = []
            
            for item in value as! NSArray {
                
                if item is String{
                    array.append(item)
                }
                else {
                    array.append(processObjectItem(item as! NSDictionary, forKey:key))
                }
            }
            
            setValue(array, forKey: key)
        }
        else {
        
            setValue(value, forKey: key)
        }
    }
    
    
    func processObjectItem(value: NSDictionary, forKey key: String) -> AnyObject {
        
        var obj : NSObject!
        
        let klass : NSObject.Type? = (self as! Serializable).getArrayClass(key)
        
        if klass != nil {
            obj = klass!.fromJson(value)
        }
        
        return obj
    }
    
    
    
    func processObject(value: AnyObject?, forKey key: String) {
        
        let aClass : AnyClass? = self.dynamicType
        let prop : objc_property_t = class_getProperty(aClass, key)
            
        let type = property_getAttributes(prop);
        let typeString : String = String(UTF8String: type)!
        let attributes : NSArray = typeString.componentsSeparatedByString(",")
        let typeAttribute : String = attributes.objectAtIndex(0) as! String
//        let prefix = NSBundle.mainBundle().infoDictionary!["CFBundleName"] as! String
        
        let propertyType : String = typeAttribute.substringFromIndex(typeAttribute.startIndex.advancedBy(1))
//        propertyType = propertyType.stringByReplacingOccurrencesOfString("@", withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil)
//        propertyType = propertyType.stringByReplacingOccurrencesOfString("\"", withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil)
//        propertyType = propertyType.stringByReplacingOccurrencesOfString("_TtC12"+prefix+"8", withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil)
//        propertyType = propertyType.stringByReplacingOccurrencesOfString("_TtC12"+prefix+"9", withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil)
//        propertyType = propertyType.stringByReplacingOccurrencesOfString("_TtC12"+prefix+"7", withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil)
//        
//        propertyType = propertyType.stringByReplacingOccurrencesOfString("_TtC9"+prefix+"9", withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil)
//
//        propertyType = propertyType.stringByReplacingOccurrencesOfString("_TtC8"+prefix+"7", withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil)
//        propertyType = propertyType.stringByReplacingOccurrencesOfString("_TtC8"+prefix+"8", withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil)
        
        let bundleID = NSBundle.mainBundle().infoDictionary!["CFBundleName"] as! String
        let propClass : NSObject.Type = NSClassFromString(bundleID + "." + propertyType) as! NSObject.Type
        let obj = propClass.fromJson(value as! NSDictionary)
            
        setValue(obj, forKey: key)
    }
    
    
    
    func asJson() -> NSDictionary {
        let json = NSMutableDictionary()
        
        for name in propertyNames() {
            if let value: AnyObject = valueForKey(name) {
                
                if value is Serializable {
                    
                    json[name] = (value as! NSObject).asJson()
                }
                else if value is [AnyObject]{
                    
                    let array : NSMutableArray = NSMutableArray()
                    
                    for item in (value as! [AnyObject]) {
                        
                        if item is Serializable {
                        
                            array.addObject((item as! NSObject).asJson())
                        }
                        else{
                            array.addObject(item)
                        }
                    }
                    
                    json[name] = array
                }
                else {
                
                    json[name] = value
                }
            }
        }
        
        
        return json
    }
    
    func asJsonData() throws -> NSData! {
        let dictionary = asJson()
        return try NSJSONSerialization.dataWithJSONObject(dictionary, options:NSJSONWritingOptions(rawValue: 0))
    }
    
    func propertyNames() -> [String] {
        var names: [String] = []
        var count: UInt32 = 0
        // Uses the Objc Runtime to get the property list
        let properties = class_copyPropertyList(classForCoder, &count)
        for i in 0 ..< Int(count) {
            let property: objc_property_t = properties[i]
            let name: String = NSString(CString: property_getName(property), encoding: NSUTF8StringEncoding) as! String
            
            names.append(name)
        }
        free(properties)
        return names
    }
    
    
    func underscoreToCamelCase(string: String) -> String {
        let items: [String] = string.componentsSeparatedByString("_")
        var camelCase = ""
        var isFirst = true
        for item: String in items {
            if isFirst == true {
                isFirst = false
                camelCase += item
            } else {
                camelCase += item.capitalizedString
            }
        }
        return camelCase
    }
}