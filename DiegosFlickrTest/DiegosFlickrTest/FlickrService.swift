//
//  FlickrService.swift
//  DiegosFlickrTest
//
//  Created by Diego Belli Mondego on 9/20/16.
//  Copyright © 2016 Bradesco. All rights reserved.
//
import UIKit

class FlickrService: Service {

    
    func search(tags: String, callback: (FlickrResp?) -> ()){
        
        let request = getRquest(tags.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLHostAllowedCharacterSet())!)
        
        execRequest(request, type: FlickrResp.self, callback: callback)
    }
}
