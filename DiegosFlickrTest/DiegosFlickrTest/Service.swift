//
//  Service.swift
//  Bradesco
//
//  Created by Diego Belli Mondego on 10/04/15.
//
//  ESTA CLASSE EU DESENVOLVI HÁ UM TEMPO ATRÁS PARA FAZER CHAMADAS WS REST. ELA IMPLEMENTA A CHAMADA DE FORMA GENERICA,
//  PERMITINDO QUE EU USE EM PRATICAMENTE TODOS OS SERVICOS.
//
import Foundation


class Service{
    
    
    func getRquest(sufixo: String) -> NSMutableURLRequest{
        
        let url = URL_BASE + sufixo
        
        let request : NSMutableURLRequest = NSMutableURLRequest()
        request.URL = NSURL(string: url)
        request.HTTPMethod = "GET"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        return request
    }
    
    
    func execRequest<T: Serializable>(request: NSMutableURLRequest, type: NSObject.Type?, callback: ((T?) -> ())?){
        
    
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request, completionHandler: {data, response, error -> Void in
            
            if error != nil {
                
                //sem tratamento de erro por hora, mas poderiamos implementar um callback para o business tratar o erro
                print(error)
            }
            else{
                
                do{
                    
                    let jsonResult: NSDictionary! = try NSJSONSerialization.JSONObjectWithData(data!, options:NSJSONReadingOptions.MutableContainers) as? NSDictionary
                    
                    if (jsonResult != nil) {
                        
                        //remover para testar
                        dispatch_async(dispatch_get_main_queue(), {
                            
                            if type != nil{
                                
                                var data : T!
                                
                                //como nesse exemplo estamos contemplando apenas a api da flickr e para evitar mais classes, estou recuperando o objeto "photos" para efetuar o parse
                                
                                if(!(jsonResult.objectForKey("photos") is NSNull)){
                                    
                                    data = type!.fromJson(jsonResult.objectForKey("photos") as! NSDictionary) as! T
                                }
                                
                                if callback != nil {
                                    callback!(data)
                                }
                            }
                        })
                        
                    } else {
                        
                        //erro no parse do json
                        print(error)
                    }
                }
                catch{
                    print(error)
                }
            }
        })
        
        task.resume()
    }
}