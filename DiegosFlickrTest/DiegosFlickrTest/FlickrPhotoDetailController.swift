//
//  PhotoDetailController.swift
//  DiegosFlickrTest
//
//  Created by Diego Belli Mondego on 9/20/16.
//  Copyright © 2016 Bradesco. All rights reserved.
//

import UIKit

class FlickrPhotoDetailController: UIViewController {

    
    @IBOutlet weak var imgPhoto: UIImageView!
    
    @IBOutlet weak var lblDesc: UILabel!
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var photo: FlickrPhoto!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if photo.title != nil {
            lblDesc.text = photo.title
        }
        
        let url: NSURL = FlickrUtils.flickrImageURL(photo, size: "b")
        
        if let image = url.cachedImage {
            imgPhoto.image = image
            imgPhoto.alpha = 1
            
            activityIndicator.hidden = true
        } else {
            
            imgPhoto.alpha = 0
            
            url.fetchImage { image in
                self.imgPhoto.image = image
                self.activityIndicator.hidden = true
                UIView.animateWithDuration(0.3) {
                    self.imgPhoto.alpha = 1
                    
                }
            }
        }
    }
}
