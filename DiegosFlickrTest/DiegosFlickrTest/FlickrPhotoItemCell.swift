//
//  FlickrPhotoItemCell.swift
//  DiegosFlickrTest
//
//  Created by Diego Belli Mondego on 9/20/16.
//  Copyright © 2016 Bradesco. All rights reserved.
//

import UIKit

class FlickrPhotoItemCell: UICollectionViewCell {
    
    
    @IBOutlet weak var imgPhoto: UIImageView!
    
    @IBOutlet weak var lblDesc: UILabel!
    
    
    func process(photo:FlickrPhoto){
        
        lblDesc.text = photo.title
        
        let url: NSURL = FlickrUtils.flickrImageURL(photo, size: "t")
        
        if let image = url.cachedImage {
            imgPhoto.image = image
            imgPhoto.alpha = 1
        } else {
            
            imgPhoto.alpha = 0
            
            url.fetchImage { image in
                self.imgPhoto.image = image
                UIView.animateWithDuration(0.3) {
                    self.imgPhoto.alpha = 1
                }
            }
        }
    }
}
