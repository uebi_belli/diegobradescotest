//
//  FlickrUtils.swift
//  DiegosFlickrTest
//
//  Created by Diego Belli Mondego on 9/20/16.
//  Copyright © 2016 Bradesco. All rights reserved.
//

import Foundation

class FlickrUtils: NSObject {

    class func flickrImageURL(photo: FlickrPhoto, size: String = "m") -> NSURL {
        
        let urlStr = String(format: "http://farm%i.staticflickr.com/%@/%@_%@_%@.jpg", photo.farm, photo.server!, photo.id!, photo.secret!, size)
        return NSURL(string: urlStr)!
    }
}
