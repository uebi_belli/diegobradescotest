//
//  FlickrListController.swift
//  DiegosFlickrTest
//
//  Created by Diego Belli Mondego on 9/20/16.
//  Copyright © 2016 Bradesco. All rights reserved.
//

import UIKit

class FlickrListController: UIViewController, UITextFieldDelegate, UICollectionViewDelegate, UICollectionViewDataSource {
    
    
    @IBOutlet weak var txtSearch: UITextField!
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    
    var searchTimer: NSTimer?
    
    var searchQuery: String!
    
    var photos: [FlickrPhoto] = []
    
    var selPhoto: FlickrPhoto!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        txtSearch.delegate = self
        
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.backgroundColor = UIColor.clearColor()
        collectionView.registerNib(UINib(nibName: "FlickrPhotoItemCell", bundle: nil), forCellWithReuseIdentifier: "FlickrPhotoItemCell")
        
        txtSearch.text = "Bradesco"
        searchQuery = txtSearch.text
        search()
    }
    
    
    func search(){
    
        activityIndicator.hidden = false
        
        FlickrService().search(searchQuery) { (resp: FlickrResp?) in
        
            self.activityIndicator.hidden = true
            
            self.collectionView.scrollRectToVisible(CGRectMake(0, 0, 0, 0), animated: false)
            self.photos = resp!.photo!
            self.collectionView.reloadData()
        }
    }
    
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if segue.identifier == "photo_detail"{
            
            (segue.destinationViewController as! FlickrPhotoDetailController).photo = selPhoto
        }
    }
    
    
    // MARK: - collection view data source / delegate
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return photos.count
    }
    
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("FlickrPhotoItemCell", forIndexPath: indexPath) as! FlickrPhotoItemCell
        
        cell.process(photos[indexPath.row])
        
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return CGSizeMake(145, 210)
    }
    
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        
        selPhoto = photos[indexPath.row]
        self.performSegueWithIdentifier("photo_detail", sender: self)
    }
    
    
    
    
    //MARK: UITextField delegate
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        
        
        //efetua a busca quando o parar de digitar e digitar ao menos 4 carcteres
        if textField.text!.characters.count > 2 {
            
            if searchTimer != nil {
                searchTimer!.invalidate()
            }
            
            searchQuery = textField.text! + string
            searchTimer = NSTimer.scheduledTimerWithTimeInterval(0.3, target: self, selector: #selector(FlickrListController.search), userInfo: nil, repeats: false)
        }
        
        return true
    }
    
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        
        return false
    }
}
