//
//  FlickrResp.swift
//  DiegosFlickrTest
//
//  Created by Diego Belli Mondego on 9/20/16.
//  Copyright © 2016 Bradesco. All rights reserved.
//
import Foundation


//objeto Photos da FLickr. Estou declarando apenas os atributos que vamos utilizar
class FlickrResp: Serializable {
    
    
    var photo : [FlickrPhoto]?
    
    //metodo necessario para o parser de arrays
    override func getArrayClass(key: String) -> NSObject.Type? {
        
        var klass : NSObject.Type!
        
        if key == "photo" {
            klass = FlickrPhoto.self
        }
        
        return klass
    }
}
