//
//  FlickrPhoto.swift
//  DiegosFlickrTest
//
//  Created by Diego Belli Mondego on 9/20/16.
//  Copyright © 2016 Bradesco. All rights reserved.
//

import Foundation

//objeto Photo da FLickr. Estou declarando apenas os atributos que vamos utilizar
class FlickrPhoto: Serializable {

    var id : String?
    
    var farm : Int = 0
    
    var server : String?
    
    var secret : String?
    
    var title : String?
}
